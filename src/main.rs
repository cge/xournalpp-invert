extern crate clap;
extern crate flate2;
extern crate quick_xml;
extern crate regex;
extern crate hex;

use quick_xml::events::attributes::Attribute;
use quick_xml::events::Event;
use quick_xml::Reader;
use quick_xml::Writer;

use regex::Regex;
use std::fs::File;
use std::io::prelude::*;

use flate2::read::GzDecoder;
use flate2::write::GzEncoder;
use flate2::Compression;
use std::error::Error;
use std::fs;
use std::io::BufReader;
use std::io::Cursor;
use palette::{Pixel, Srgb, Lab, Hsv, LinSrgb};

use clap::{App, Arg};

fn rep_color(mut attr: Attribute) -> Attribute {
    if attr.key == b"color" {
        let mut p1 = [0; 3];
        let b:Vec<u8> = hex::decode(attr.value[1 .. 7].to_vec()).unwrap();
        p1.copy_from_slice(&b);
        let xa : &Srgb<u8>= Pixel::from_raw(&p1);
        let x:LinSrgb<f64> = Srgb::from_format(*xa).into_linear();
        let mut y = Lab::from(x);
        y.l = 100.0 - y.l;
        let pixel: [u8; 3] = Srgb::from_linear(LinSrgb::from(y)).into_format().into_raw();
        let z = attr.value.to_mut();
        *z = format!("#{:}ff", hex::encode(pixel)).as_bytes().to_vec();
        }
    return attr;
}


fn main() -> Result<(), Box<dyn Error>> {
    let matches = App::new("xournalpp-invert")
        .version("0.1")
        .about("Inverts brightness (using Lab) colors in a xournalpp file.")
        .author("Constantine Evans")
        .arg(Arg::with_name("INPUT_FILE")
             .help("Input file")
             .required(true)
             .index(1))
        .get_matches();

    let filename = matches.value_of("INPUT_FILE").unwrap();

    let mut input = fs::File::open(filename)?;
    let mut decoder = GzDecoder::new(&mut input);

    let bufread = BufReader::new(&mut decoder);
    let mut reader = Reader::from_reader(bufread);
    reader.trim_text(true);
    let mut writer = Writer::new(Cursor::new(Vec::new()));
    let mut buf = Vec::new();
    loop {
        match reader.read_event(&mut buf) {
            Ok(Event::Start(ref e)) => {
                let mut elem = e.to_owned();
                elem.clear_attributes()
                    .extend_attributes(e.attributes()
                                       .map(|attr| rep_color(attr.unwrap())));
                assert!(writer.write_event(Event::Start(elem)).is_ok());
            }
            Ok(Event::Empty(ref e)) => {
                let mut elem = e.to_owned();
                elem.clear_attributes()
                    .extend_attributes(e.attributes()
                                       .map(|attr| rep_color(attr.unwrap())));
                assert!(writer.write_event(Event::Empty(elem)).is_ok());
            }
            Ok(Event::Eof) => break,
            Ok(e) => assert!(writer.write_event(&e).is_ok()),
            Err(e) => panic!("Error at position {}: {:?}", reader.buffer_position(), e),
            //Err(e) => return Result::Err(Box::new(e))
        }
        //buf.clear();
    }
    let result = writer.into_inner().into_inner();

    let re = Regex::new(r"^(?P<fn>.*)\.xopp")?;
    let a = re.replace_all(filename, "$fn-inverse.xopp");

    let f = File::create(&a.into_owned())?;
    let mut encoder = GzEncoder::new(f, Compression::default());
    encoder.write_all(&result)?;
    encoder.finish()?;

    Ok(())
}
